<?php

class Autoloader
{

    /**
     * Ajoute loadClass à la pile d'execution
     */
    public static function register()
    {
        spl_autoload_register('self::loadClass');
    }

    /**
     * @param $classe
     */
    public static function loadClass($classe)
    {
        if (preg_match('#Controller$#', $classe)) {
            if (file_exists('Controller/'.$classe.'.php')) {
                include 'Controller/'.$classe.'.php';
            }
        } elseif (preg_match('#Manager$#', $classe)) {
            if (file_exists('Model/Manager/'.$classe.'.php')) {
                include 'Model/Manager/'.$classe.'.php';
            }
        } elseif (preg_match('#Router#', $classe)) {
            if (file_exists('Model/Router/'.$classe.'.php')) {
                include 'Model/Router/'.$classe.'.php';
            }
        } elseif (preg_match('#View#', $classe)) {
            if (file_exists('View/'.$classe.'.php')) {
                include 'View/'.$classe.'.php';
            }
        } else {
            if (file_exists('Model/Entity/'.$classe.'.php')) {
                include 'Model/Entity/'.$classe.'.php';
            }
        }
    }
}
