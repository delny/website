<?php
  
  namespace Website\Model\Manager;
  
class Manager extends DatabaseManager
{
    private $bdd;
    
    /**
     * Manager Constructor.
     */
    public function __construct()
    {
        $this->bdd = parent::getBDD();
    }
    
    /**
     * @return mixed
     */
    public function getBonjour()
    {
      
        $id = rand(1, 20);
        $sql = $this->bdd->prepare('SELECT * FROM bonjour WHERE id = :id');
        $sql->bindValue(':id', $id);
        $sql->execute();
      
        return $sql->fetch();
    }
}
