<?php
  $titre = WEBSITE_TITLE.' - Accueil';
?>

<?php ob_start(); ?>
  <h1><?php echo $bonjour; ?><sup>&#42;</sup></h1>
  <p><sup>&#42;</sup>Bonjour et bienvenue en <?php echo $bonjour_l; ?></p>

  <p> Développeur back-end, j'ai dabord codé sans framework en PHP5,
    et ensuite utilisant Symfony puis Drupal.
  </p>
  <!--[if lt IE 9]>
  <p style="color:#FF1212;">Pour une navigation optimale sur ce site, je vous invite à mettre à jour votre navigateur
    Internet Explorer.</p>
  <![endif]-->
  <a href="page-parcours.html" class="button">Voir mon parcours</a>
  <?php $contenu = ob_get_clean(); ?>

<?php
  $donnees_vue = array(
    "titre" => $titre,
    "contenu" => $contenu
  );
