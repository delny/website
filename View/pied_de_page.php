<footer class="responsive-blog-footer">
  <div class="row">
    <div class="columns flex-container align-justify">
      <div>
        <p>anthonydelgado.fr © 2019</p>
      </div>
      <div>
        <p>Derni&egrave;re mise &agrave; jour : 9 juin 2019</p>
      </div>
      <ul class="vertical menu simple">
        <li><a href="page-credits.html">Mentions l&eacute;gales</a></li>
        <li><a href="page-contact.html">Contact</a></li>
      </ul>
    </div>
  </div>
</footer>
<script src="View/js/vendor/jquery.js"></script>
<script src="View/js/vendor/what-input.js"></script>
<script src="node_modules/foundation-sites/dist/js/foundation.js"></script>
<script type="text/javascript" src="View/js/app.js"></script>