<!DOCTYPE html>
<html>
<head>
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <title><?php echo $titre; ?></title>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width"/>
  <meta name="robots" content="noindex"/>
  <link rel="shortcut icon" type="image/x-icon" href="https://www.anthonydelgado.fr/View/img/favicon.ico"/>
  <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <link rel="stylesheet" href="node_modules/foundation-sites/dist/css/foundation.css">
  <link rel="stylesheet" href="View/css/main.css"/>

</head>
<body>
<!-- script google analytics -->
<script>
  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
      m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

  ga('create', 'UA-40930967-1', 'anthonydelgado.fr');
  ga('send', 'pageview');

</script>
<!-- fin script google analytics -->
<?php include("menu.php"); ?>
<div class="corps container">
  <?php echo $contenu; ?>
</div>
<?php include("pied_de_page.php"); ?>
<script type="text/javascript">
    var el = document.createElement('script');
    el.setAttribute('src', 'https://static.axept.io/sdk.js');
    el.setAttribute('type', 'text/javascript');
    el.setAttribute('async', true);
    el.setAttribute('data-id', '5e0200ed730c99249ab73687');
    if (document.body !== null) {
        document.body.appendChild(el);
    }
</script>
</body>
</html>
