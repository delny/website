//JS
var gulp = require('gulp');
// Requires the gulp-sass plugin
var sass = require('gulp-sass');
var csso = require('gulp-csso');

gulp.task('sass', function(){
    return gulp.src('scss/main.scss')
      .pipe(sass({
        outputStyle: 'nested',
        precision: 10,
        includePaths: ['.'],
        onError: console.error.bind(console, 'Sass error:')
      }))
      .pipe(csso())
      .pipe(gulp.dest('View/css'))
  });