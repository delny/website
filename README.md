# website

repository for my website :  
http://www.anthonydelgado.fr

# Installation

Récupérer le projet
```
git clone git@gitlab.com:delny/website.git
cd website
```

Installer les dépendances
```

npm install
```

Ajouter le fichier de configuration
```
cp Config/parameters.php.dist Config/parameters.php
```


# Assets

```
gulp sass
```