<?php
namespace Deployer;

require 'recipe/composer.php';

// Project name
set('application', 'Website');
set('directory', 'website');
set('bin/php', function () {
    return '/usr/local/php7.4/bin/php';
});
set('bin_dir', 'bin');

// Project repository
set('repository', 'git@gitlab.com:delny/website.git');

// Shared files/dirs between deploys 
set('shared_files', ['Config/parameters.php']);
set('shared_dirs', ['View/img']);

// Writable dirs by web server 
set('writable_dirs', []);

// Hosts
inventory('deploy/hosts.yml'); 

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

